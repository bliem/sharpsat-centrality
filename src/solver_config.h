/*
 * basic_types.h
 *
 *  Created on: Jun 24, 2012
 *      Author: Marc Thurley
 */

#ifndef SOLVER_CONFIG_H_
#define SOLVER_CONFIG_H_


struct SolverConfiguration {

  bool perform_non_chron_back_track = true;

  // TODO component caching cannot be deactivated for now!
  bool perform_component_caching = true;
  bool perform_failed_lit_test = true;
  bool perform_pre_processing = true;
  bool centrality_forgetting  = false;

  unsigned long time_bound_seconds = 100000;

  bool verbose = false;

  // quiet = true will override verbose;
  bool quiet = false;

  bool init_activity_zero = false;

  float freq_score_weight = 1.0;
  float activity_score_weight = 10.0;
  float var_centrality_weight = 1.0;
  float var_centrality_decay = 1; // centrality scores are multiplied by var_centrality_decay^decision_level
  bool var_centrality_scaling = true;
};

#endif /* SOLVER_CONFIG_H_ */
