/*
 * instance.cpp
 *
 *  Created on: Aug 23, 2012
 *      Author: Marc Thurley
 */

#include "instance.h"

#include <algorithm>
#include <fstream>
#include <sys/stat.h>
#include <sstream>

//#include <Python.h>
#include "pybind11/embed.h"
namespace py = pybind11;

using namespace std;

void Instance::cleanClause(ClauseOfs cl_ofs) {
  bool satisfied = false;
  for (auto it = beginOf(cl_ofs); *it != SENTINEL_LIT; it++)
    if (isSatisfied(*it)) {
      satisfied = true;
      break;
    }
  // mark the clause as empty if satisfied
  if (satisfied) {
    *beginOf(cl_ofs) = SENTINEL_LIT;
    return;
  }
  auto jt = beginOf(cl_ofs);
  auto it = beginOf(cl_ofs);
  // from now, all inactive literals are resolved
  for (; *it != SENTINEL_LIT; it++, jt++) {
    while (*jt != SENTINEL_LIT && !isActive(*jt))
      jt++;
    *it = *jt;
    if (*jt == SENTINEL_LIT)
      break;
  }
  unsigned length = it - beginOf(cl_ofs);
  // if it has become a unit clause, it should have already been asserted
  if (length == 1) {
    *beginOf(cl_ofs) = SENTINEL_LIT;
    // if it has become binary, transform it to binary and delete it
  } else if (length == 2) {
    addBinaryClause(*beginOf(cl_ofs), *(beginOf(cl_ofs) + 1));
    *beginOf(cl_ofs) = SENTINEL_LIT;
  }
}

void Instance::compactClauses() {
  vector<ClauseOfs> clause_ofs;
  clause_ofs.reserve(statistics_.num_long_clauses_);

  // clear watch links and occurrence lists
  for (auto it_lit = literal_pool_.begin(); it_lit != literal_pool_.end();
      it_lit++) {
    if (*it_lit == SENTINEL_LIT) {
      if (it_lit + 1 == literal_pool_.end())
        break;
      it_lit += ClauseHeader::overheadInLits();
      clause_ofs.push_back(1 + it_lit - literal_pool_.begin());
    }
  }

  for (auto ofs : clause_ofs)
    cleanClause(ofs);

  for (auto &l : literals_)
    l.resetWatchList();

  occurrence_lists_.clear();
  occurrence_lists_.resize(variables_.size());

  vector<LiteralID> tmp_pool = literal_pool_;
  literal_pool_.clear();
  literal_pool_.push_back(SENTINEL_LIT);
  ClauseOfs new_ofs;
  unsigned num_clauses = 0;
  for (auto ofs : clause_ofs) {
    auto it = (tmp_pool.begin() + ofs);
    if (*it != SENTINEL_LIT) {
      for (unsigned i = 0; i < ClauseHeader::overheadInLits(); i++)
        literal_pool_.push_back(0);
      new_ofs = literal_pool_.size();
      literal(*it).addWatchLinkTo(new_ofs);
      literal(*(it + 1)).addWatchLinkTo(new_ofs);
      num_clauses++;
      for (; *it != SENTINEL_LIT; it++) {
        literal_pool_.push_back(*it);
        occurrence_lists_[*it].push_back(new_ofs);
      }
      literal_pool_.push_back(SENTINEL_LIT);
    }
  }

  vector<LiteralID> tmp_bin;
  unsigned bin_links = 0;
  for (auto &l : literals_) {
    tmp_bin.clear();
    for (auto it = l.binary_links_.begin(); *it != SENTINEL_LIT; it++)
      if (isActive(*it))
        tmp_bin.push_back(*it);
    bin_links += tmp_bin.size();
    tmp_bin.push_back(SENTINEL_LIT);
    l.binary_links_ = tmp_bin;
  }
  statistics_.num_long_clauses_ = num_clauses;
  statistics_.num_binary_clauses_ = bin_links >> 1;
}

void Instance::compactVariables() {
  vector<unsigned> var_map(variables_.size(), 0);
  unsigned last_ofs = 0;
  unsigned num_isolated = 0;
  LiteralIndexedVector<vector<LiteralID> > _tmp_bin_links(1);
  LiteralIndexedVector<TriValue> _tmp_values = literal_values_;

  for (auto l : literals_)
    _tmp_bin_links.push_back(l.binary_links_);

  assert(_tmp_bin_links.size() == literals_.size());
  for (unsigned v = 1; v < variables_.size(); v++)
    if (isActive(v)) {
      if (isolated(v)) {
        num_isolated++;
        continue;
      }
      last_ofs++;
      var_map[v] = last_ofs;
    }

  variables_.clear();
  variables_.resize(last_ofs + 1);
  occurrence_lists_.clear();
  occurrence_lists_.resize(variables_.size());
  literals_.clear();
  literals_.resize(variables_.size());
  literal_values_.clear();
  literal_values_.resize(variables_.size(), X_TRI);

  assert(centralities.empty());
  centralities.resize(variables_.size(), 0.0);

  unsigned bin_links = 0;
  LiteralID newlit;
  for (auto l = LiteralID(0, false); l != _tmp_bin_links.end_lit(); l.inc()) {
    if (var_map[l.var()] != 0) {
      newlit = LiteralID(var_map[l.var()], l.sign());
      for (auto it = _tmp_bin_links[l].begin(); *it != SENTINEL_LIT; it++) {
        assert(var_map[it->var()] != 0);
        literals_[newlit].addBinLinkTo(
            LiteralID(var_map[it->var()], it->sign()));
      }
      bin_links += literals_[newlit].binary_links_.size() - 1;
    }
  }

  vector<ClauseOfs> clause_ofs;
  clause_ofs.reserve(statistics_.num_long_clauses_);
  // clear watch links and occurrence lists
  for (auto it_lit = literal_pool_.begin(); it_lit != literal_pool_.end();
      it_lit++) {
    if (*it_lit == SENTINEL_LIT) {
      if (it_lit + 1 == literal_pool_.end())
        break;
      it_lit += ClauseHeader::overheadInLits();
      clause_ofs.push_back(1 + it_lit - literal_pool_.begin());
    }
  }

  for (auto ofs : clause_ofs) {
    literal(LiteralID(var_map[beginOf(ofs)->var()], beginOf(ofs)->sign())).addWatchLinkTo(
        ofs);
    literal(LiteralID(var_map[(beginOf(ofs) + 1)->var()],
            (beginOf(ofs) + 1)->sign())).addWatchLinkTo(ofs);
    for (auto it_lit = beginOf(ofs); *it_lit != SENTINEL_LIT; it_lit++) {
      *it_lit = LiteralID(var_map[it_lit->var()], it_lit->sign());
      occurrence_lists_[*it_lit].push_back(ofs);
    }
  }

  literal_values_.clear();
  literal_values_.resize(variables_.size(), X_TRI);
  unit_clauses_.clear();

  statistics_.num_variables_ = variables_.size() - 1 + num_isolated;

  statistics_.num_used_variables_ = num_variables();
  statistics_.num_free_variables_ = num_isolated;
}

void Instance::compactConflictLiteralPool(){
  auto write_pos = conflict_clauses_begin();
  vector<ClauseOfs> tmp_conflict_clauses = conflict_clauses_;
  conflict_clauses_.clear();
  for(auto clause_ofs: tmp_conflict_clauses){
    auto read_pos = beginOf(clause_ofs) - ClauseHeader::overheadInLits();
    for(unsigned i = 0; i < ClauseHeader::overheadInLits(); i++)
      *(write_pos++) = *(read_pos++);
    ClauseOfs new_ofs =  write_pos - literal_pool_.begin();
    conflict_clauses_.push_back(new_ofs);
    // first substitute antecedent if clause_ofs implied something
    if(isAntecedentOf(clause_ofs, *beginOf(clause_ofs)))
      var(*beginOf(clause_ofs)).ante = Antecedent(new_ofs);

    // now redo the watches
    literal(*beginOf(clause_ofs)).replaceWatchLinkTo(clause_ofs,new_ofs);
    literal(*(beginOf(clause_ofs)+1)).replaceWatchLinkTo(clause_ofs,new_ofs);
    // next, copy clause data
    assert(read_pos == beginOf(clause_ofs));
    while(*read_pos != SENTINEL_LIT)
      *(write_pos++) = *(read_pos++);
    *(write_pos++) = SENTINEL_LIT;
  }
  literal_pool_.erase(write_pos,literal_pool_.end());
}


//bool Instance::deleteConflictClauses() {
//  statistics_.times_conflict_clauses_cleaned_++;
//  vector<ClauseOfs> tmp_conflict_clauses = conflict_clauses_;
//  conflict_clauses_.clear();
//  vector<double> tmp_ratios;
//  double score, lifetime;
//  for(auto clause_ofs: tmp_conflict_clauses){
//    score = getHeaderOf(clause_ofs).score();
//    lifetime = statistics_.num_conflicts_ - getHeaderOf(clause_ofs).creation_time();
//    tmp_ratios.push_back(score/lifetime/(getHeaderOf(clause_ofs).length()));
//  }
//  vector<double> tmp_ratiosB = tmp_ratios;
//
//  sort(tmp_ratiosB.begin(), tmp_ratiosB.end());
//
//  double cutoff = tmp_ratiosB[tmp_ratiosB.size()/2];
//
//  for(unsigned i = 0; i < tmp_conflict_clauses.size(); i++){
//    if(tmp_ratios[i] < cutoff){
//      if(!markClauseDeleted(tmp_conflict_clauses[i]))
//        conflict_clauses_.push_back(tmp_conflict_clauses[i]);
//    } else
//      conflict_clauses_.push_back(tmp_conflict_clauses[i]);
//  }
//  return true;
//}

bool Instance::deleteConflictClauses(bool use_centrality) {
  statistics_.times_conflict_clauses_cleaned_++;
  vector<ClauseOfs> tmp_conflict_clauses = conflict_clauses_;
  conflict_clauses_.clear();

  if(use_centrality) {
	  vector<float> scores;
	  for(auto clause_ofs: tmp_conflict_clauses) {
		  // The score of the clause is the average centrality score of its variables.
		  // clause_ofs is the offset in literal_pool_ where the clause starts.
		  float score = 0.0;
		  for(auto it = literal_pool_.begin() + clause_ofs; *it != SENTINEL_LIT; ++it) {
			  score += centralities[it->var()];
		  }
		  score /= getHeaderOf(clause_ofs).length();
		  scores.push_back(score);
	  }

	  vector<float> sorted_scores = scores;
	  sort(sorted_scores.begin(), sorted_scores.end());
	  // Median score
	  float cutoff = sorted_scores[sorted_scores.size()/2];

	  // Only keep clauses whose score is at least the median
	  for(unsigned i = 0; i < tmp_conflict_clauses.size(); i++){
		  if(scores[i] < cutoff){
			  if(!markClauseDeleted(tmp_conflict_clauses[i]))
				  conflict_clauses_.push_back(tmp_conflict_clauses[i]);
		  } else
			  conflict_clauses_.push_back(tmp_conflict_clauses[i]);
	  }
  }
  else {
	  vector<double> tmp_ratios;
	  double score, lifetime;
	  for(auto clause_ofs: tmp_conflict_clauses){
		  score = getHeaderOf(clause_ofs).score();
		  lifetime = statistics_.num_conflicts_ - getHeaderOf(clause_ofs).creation_time();
		  // tmp_ratios.push_back(score/lifetime);
		  tmp_ratios.push_back(score);

	  }
	  vector<double> tmp_ratiosB = tmp_ratios;

	  sort(tmp_ratiosB.begin(), tmp_ratiosB.end());

	  // Median score
	  double cutoff = tmp_ratiosB[tmp_ratiosB.size()/2];

	  // Only keep clauses whose score is lower than the median
	  for(unsigned i = 0; i < tmp_conflict_clauses.size(); i++){
		  if(tmp_ratios[i] < cutoff){
			  if(!markClauseDeleted(tmp_conflict_clauses[i]))
				  conflict_clauses_.push_back(tmp_conflict_clauses[i]);
		  } else
			  conflict_clauses_.push_back(tmp_conflict_clauses[i]);
	  }
  }
  return true;
}


bool Instance::markClauseDeleted(ClauseOfs cl_ofs){
  // only first literal may possibly have cl_ofs as antecedent
  if(isAntecedentOf(cl_ofs, *beginOf(cl_ofs)))
    return false;

  literal(*beginOf(cl_ofs)).removeWatchLinkTo(cl_ofs);
  literal(*(beginOf(cl_ofs)+1)).removeWatchLinkTo(cl_ofs);
  return true;
}


bool Instance::createfromFile(const string &file_name) {
  unsigned int nVars, nCls;
  int lit;
  unsigned max_ignore = 1000000;
  unsigned clauses_added = 0;
  LiteralID llit;
  vector<LiteralID> literals;
  string idstring;
  char c;

  // clear everything
  literal_pool_.clear();
  literal_pool_.push_back(SENTINEL_LIT);

  variables_.clear();
  variables_.push_back(Variable()); //initializing the Sentinel
  literal_values_.clear();
  unit_clauses_.clear();

  ///BEGIN File input
  ifstream input_file(file_name);
  if (!input_file) {
    cerr << "Cannot open file: " << file_name << endl;
    exit(0);
  }

  struct stat filestatus;
  stat(file_name.c_str(), &filestatus);

  literals.reserve(10000);
  while (input_file >> c && c != 'p')
    input_file.ignore(max_ignore, '\n');
  if (!(input_file >> idstring && idstring == "cnf" && input_file >> nVars
      && input_file >> nCls)) {
    cerr << "Invalid CNF file" << endl;
    exit(0);
  }

  variables_.resize(nVars + 1);
  literal_values_.resize(nVars + 1, X_TRI);
  literal_pool_.reserve(filestatus.st_size);
  conflict_clauses_.reserve(2*nCls);
  occurrence_lists_.clear();
  occurrence_lists_.resize(nVars + 1);

  literals_.clear();
  literals_.resize(nVars + 1);

  while ((input_file >> c) && clauses_added < nCls) {
    input_file.unget(); //extracted a nonspace character to determine if we have a clause, so put it back
    if ((c == '-') || isdigit(c)) {
      literals.clear();
      bool skip_clause = false;
      while ((input_file >> lit) && lit != 0) {
        bool duplicate_literal = false;
        for (auto i : literals) {
          if (i.toInt() == lit) {
            duplicate_literal = true;
            break;
          }
          if (i.toInt() == -lit) {
            skip_clause = true;
            break;
          }
        }
        if (!duplicate_literal) {
          literals.push_back(lit);
        }
      }
      if (!skip_clause) {
        assert(!literals.empty());
        clauses_added++;
        statistics_.incorporateClauseData(literals);
        ClauseOfs cl_ofs = addClause(literals);
        if (literals.size() >= 3)
          for (auto l : literals)
            occurrence_lists_[l].push_back(cl_ofs);
      }
    }
    input_file.ignore(max_ignore, '\n');
  }
  ///END NEW
  input_file.close();
  //  /// END FILE input

  statistics_.num_variables_ = statistics_.num_original_variables_ = nVars;
  statistics_.num_used_variables_ = num_variables();
  statistics_.num_free_variables_ = nVars - num_variables();

  statistics_.num_original_clauses_ = nCls;

  statistics_.num_original_binary_clauses_ = statistics_.num_binary_clauses_;
  statistics_.num_original_unit_clauses_ = statistics_.num_unit_clauses_ =
      unit_clauses_.size();

  original_lit_pool_size_ = literal_pool_.size();
  return true;
}

void Instance::computeCentrality(bool quiet, bool scale) {
	py::scoped_interpreter guard{};
	py::object g = py::module::import("networkx").attr("Graph")();

	// Add nodes to graph
	for(auto l = LiteralID(1, false); l != literals_.end_lit(); l.inc()) {
		g.attr("add_node")(l.var());
	}

	// Add binary clause edges
	for(auto l = LiteralID(1, false); l != literals_.end_lit(); l.inc()) {
		for(auto it = literal(l).binary_links_.begin(); *it != SENTINEL_LIT; ++it) {
			// If the literal *it has a smaller index than l, make an edge
			// (We check that the index is smaller to avoid duplicates.)
			if(it->var() < l.var())
				g.attr("add_edge")(it->var(), l.var());
		}
	}

	// Add long clause edges
	vector<unsigned> currentClause;
	for(vector<LiteralID>::const_iterator it = literal_pool_.begin(); it != literal_pool_.end(); ++it) {
		const LiteralID lit = *it;
		if(lit == SENTINEL_LIT)
			currentClause.clear();
		else {
			if(currentClause.size() > 0) {
				for(auto v : currentClause) {
					g.attr("add_edge")(v, lit.var());
				}
			}
			currentClause.push_back(lit.var());
		}
	}

	py::object betweenness_centrality =
		py::module::import("networkx.algorithms.centrality").attr("betweenness_centrality");

	// Jamali and Mitchell approximated using n/50 samples
	const unsigned nVars = literals_.end_lit().var() - 1;
	py::dict result;
	//if(nVars >= 1000)
	//    result = betweenness_centrality(g, nVars / 50);
	const unsigned approximation_start = 400; // Exact computation if nodes are less than this number
	const unsigned max_nodes = 2*approximation_start; // Approximate with at most this number of nodes
	if(nVars >= approximation_start) {
		const unsigned num_nodes = max_nodes - approximation_start*approximation_start / nVars;
		if(!quiet)
			cout << " (" << num_nodes << " nodes)" << flush;
		result = betweenness_centrality(g, num_nodes);
	} else {
		if(!quiet)
			cout << " (" << nVars << " nodes)" << flush;
		result = betweenness_centrality(g);
	}
	//py::print(result);

	for(auto pair : result) {
		//py::print(pair);
		unsigned var = py::cast<unsigned>(pair.first);
		float centrality = py::cast<float>(pair.second);
		if(scale) {
			// We multiply the score by the number of clauses because VSIDS and frequence scores can be at most this as well.
			// TODO check if this is reasonable
			centrality *= statistics_.num_clauses();
		}

		//cout << var << ": " << centrality << '\n';
		assert(1 <= var && var <= nVars);
		centralities[var] = centrality;
	}

	//Py_Initialize();
    //
	//PyObject* pName = PyUnicode_FromString("networkx.algorithms.centrality");
	//PyObject* pCentralityModule = PyImport_Import(pName);
	//Py_DECREF(pName);
    //
	//if(!pCentralityModule) {
	//    // TODO error
	//    exit(1);
	//}
    //
	//pName = PyUnicode_FromString("networkx");
	//PyObject* pNetworkxModule = PyImport_Import(pName);
	//Py_DECREF(pName);
    //
	//if(!pNetworkxModule) {
	//    // TODO error
	//    exit(2);
	//}
    //
	//// Create graph object
	//PyObject* pGraphClass = PyObject_GetAttrString(pNetworkxModule, "Graph");
	//if(!pGraphClass || !PyCallable_Check(pGraphClass)) {
	//    // TODO error
	//    exit(3);
	//}
	//PyObject* pGraph = PyObject_CallObject(pGraphClass, nullptr);
	//if(!pGraph) {
	//    // TODO error
	//    exit(4);
	//}
    //
	//// Add nodes to graph
	//for(auto l = LiteralID(1, false); l != literals_.end_lit(); l.inc()) {
	//    PyObject* pResult = PyObject_CallMethod(pGraph, "add_node", "(i)", l.var());
	//    if(!pResult) {
	//        // TODO error
	//        exit(5);
	//    }
	//    Py_DECREF(pResult);
	//}
    //
	//// Add binary clause edges
	//for(auto l = LiteralID(1, false); l != literals_.end_lit(); l.inc()) {
	//    for(auto it = literal(l).binary_links_.begin(); *it != SENTINEL_LIT; ++it) {
	//        // If the literal *it has a smaller index than l, make an edge
	//        // (We check that the index is smaller to avoid duplicates.)
	//        if(it->var() < l.var()) {
	//            //program << "g.add_edge(" << it->var() << ',' << l.var() << ")\n";
	//            PyObject* pResult = PyObject_CallMethod(pGraph, "add_edge", "(ii)", it->var(), l.var());
	//            if(!pResult) {
	//                // TODO error
	//                exit(6);
	//            }
	//            Py_DECREF(pResult);
	//        }
	//    }
	//}
    //
	//// Add long clause edges
	//vector<int> currentClause;
	//for(vector<LiteralID>::const_iterator it = literal_pool_.begin(); it != literal_pool_.end(); ++it) {
	//    const LiteralID lit = *it;
	//    if(lit == SENTINEL_LIT)
	//        currentClause.clear();
	//    else {
	//        if(currentClause.size() > 0) {
	//            for(auto v : currentClause) {
	//                //program << "g.add_edge(" << v << ',' << lit.var() << ")\n";
	//                PyObject* pResult = PyObject_CallMethod(pGraph, "add_edge", "(ii)", v, lit.var());
	//                if(!pResult) {
	//                    // TODO error
	//                    exit(7);
	//                }
	//                Py_DECREF(pResult);
	//            }
	//        }
	//        currentClause.push_back(lit.var());
	//    }
	//}
    //
	//// Compute betweenness centrality
	//PyObject* pBetweennessCentrality = PyObject_GetAttrString(pCentralityModule, "betweenness_centrality");
	//if(!pBetweennessCentrality) {
	//    // TODO error
	//    exit(8);
	//}
	//PyObject* pCentralityResult = PyObject_CallFunctionObjArgs(pBetweennessCentrality, pGraph, nullptr);
	//if(!pCentralityResult) {
	//    // TODO error
	//    exit(9);
	//}
    //
	//PyObject* pBuiltins = PyEval_GetBuiltins();
	//PyObject* pPrint = PyDict_GetItemString(pBuiltins, "print");
    //
	//PyObject* pResult = PyObject_CallFunctionObjArgs(pPrint, pCentralityResult, nullptr);
	//Py_DECREF(pResult);
	//Py_DECREF(pCentralityResult);
    //
	////program << "print(betweenness_centrality(g))\n";
	////program << "print('foobar')\n";
    //
	//PyObject* pNodes = PyObject_CallMethod(pGraph, "nodes", nullptr);
	//if(!pNodes) {
	//    // TODO error
	//    exit(10);
	//}
    //
	//pResult = PyObject_CallFunctionObjArgs(pPrint, pNodes, nullptr);
	//Py_DECREF(pResult);
    //
	//Py_DECREF(pPrint);
	//Py_DECREF(pBuiltins);
	//Py_DECREF(pNodes);
	//Py_DECREF(pGraph);
	//Py_DECREF(pCentralityModule);
	//Py_DECREF(pNetworkxModule);
    //
	////PyRun_SimpleString(program.str().c_str());
	//if(Py_FinalizeEx() < 0)
	//    exit(120);
}

